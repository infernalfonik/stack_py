from rest_framework import serializers

from .models import Text, Page, Audio, Video

class TextSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=120)
    body = serializers.CharField()
    page_id = serializers.IntegerField()
    counter = serializers.IntegerField()
    def create(self, validated_data):
        return Text.objects.create(**validated_data)

class AudioSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=120)
    file = serializers.FileField()
    bitrate = serializers.IntegerField()
    page_id = serializers.IntegerField()
    counter = serializers.IntegerField()
    class Meta:
        model = Audio
        fields = "__all__"

class VideoSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=120)
    file = serializers.FileField()
    subtitles = serializers.FileField()
    page_id = serializers.IntegerField()
    counter = serializers.IntegerField()
    class Meta:
        model = Video
        fields = "__all__"

class PageSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=120)
    number = serializers.IntegerField()
    counter = serializers.IntegerField()
    def create(self, validated_data):
        return Page.objects.create(**validated_data)
