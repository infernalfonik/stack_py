from myproj.celery import app
from threading import Lock
#from django.db import transaction
lock = Lock()


@app.task
def count_views(model, pk):
    lock.acquire()
    try:
        page = model.objects.get(pk=pk)
        page.counter += 1
        page.save()
    finally:
        lock.release()
