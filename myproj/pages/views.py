import os
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.parsers import FileUploadParser
from rest_framework import status
from django.shortcuts import get_object_or_404
from .tasks import count_views

from .models import Text, Page, Audio, Video
from .serializers import TextSerializer, PageSerializer, AudioSerializer, VideoSerializer


class TextView(APIView):
    def get(self, request, pk):
        text = get_object_or_404(Text.objects.all(), pk=pk)
        serializer = TextSerializer(text)
        count_views(Text, pk)
        return Response({"text": serializer.data})

    def post(self, request):
        text = request.data.get('text')
        serializer = TextSerializer(data=text)
        if(serializer.is_valid(raise_exception=True)):
            text_saved = serializer.save()
        return Response({"succes": "Text '{}' created successfully".format(text_saved.title)})


class AllTextView(APIView):
    def get(self, request):
        text = Text.objects.all()
        serializer = TextSerializer(text, many=True)
        return Response({"text": serializer.data})

    def post(self, request):
        audio_serializer = AudioSerializer(data=request.data)
        if(audio_serializer.is_valid(raise_exception=True)):
            audio_serializer.save()
            return Response(audio_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(audio_serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class AudioUploadView(APIView):
    parser_class = (FileUploadParser,)

    def get(self, request, pk):
        audio = get_object_or_404(Audio.objects.all(), audio_id=pk)
        serializer = AudioSerializer(audio)
        count_views(Audio, pk)
        return Response({"audio": serializer.data})

    def post(self, request):
        audio_serializer = AudioSerializer(data=request.data)
        if(audio_serializer.is_valid(raise_exception=True)):
            audio_serializer.save()
            return Response(audio_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(audio_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AllAudioView(APIView):
    parser_class = (FileUploadParser,)

    def get(self, request):
        audio = Audio.objects.all()
        serializer = AudioSerializer(audio)
        return Response({"audio": serializer.data})

    def post(self, request):
        audio_serializer = AudioSerializer(data=request.data)
        if(audio_serializer.is_valid(raise_exception=True)):
            audio_serializer.save()
            return Response(audio_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(audio_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class VideoView(APIView):
    parser_class = (FileUploadParser,)

    def get(self, request, pk):
        video = get_object_or_404(Video.objects.all(), video_id=pk)
        serializer = VideoSerializer(video)
        count_views(Video, pk)
        return Response({"video": serializer.data})

    def post(self, request):
        video_serializer = VideoSerializer(data=request.data)
        if(video_serializer.is_valid(raise_exception=True)):
            video_serializer.save()
            return Response(video_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(video_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        video = get_object_or_404(Video.objects.all(), audio_id=pk)
        video.delete()
        return Response({
            "video": "Video '{}' has been deleted.".format(pk)
        }, status=204)


class AllVideoView(APIView):
    parser_class = (FileUploadParser,)

    def get(self, request):
        video = Video.objects.all()
        serializer = VideoSerializer(video, many=True)
        return Response({"videos": serializer.data})

    def delete(self, request):
        video = Video.objects.all()
        video.delete()
        return Response({
            "message": "All videos have been deleted"
        })


class PageView(APIView):
    def get(self, request, pk):
        page = get_object_or_404(Page.objects.all(), pk=pk)
        #entrylist = list(page)
        text = Text.objects.filter(page=page)
        audio = Audio.objects.filter(page=page)
        video = Video.objects.filter(page=page)
        if audio:
            count_views(Audio, pk)
        if text:
            count_views(Text, pk)
        if video:
            count_views(Video, pk)
        count_views(Page, pk)
        serializer = PageSerializer(page)
        serializer_text = TextSerializer(text, many=True)
        serializer_audio = AudioSerializer(audio, many=True)
        serializer_video = VideoSerializer(video, many=True)
        return Response({"page": serializer.data, "text": serializer_text.data, "audio": serializer_audio.data, "video": serializer_video.data})

    def post(self, request):
        page = request.data.get('page')
        serializer = PageSerializer(data=page)
        if(serializer.is_valid(raise_exception=True)):
            page_saved = serializer.save()
        return Response({"succes": "Page '{}' created successfully".format(page_saved.title)})

    
    def delete(self, request, pk):
        page = get_object_or_404(Page.objects.all(), pk=pk)
        page.delete()
        return Response({
            "message": "Page number '{}' has been deleted.".format(pk)
        }, status=204)


class AllPageView(APIView):

    def get(self, request):
        page = Page.objects.all()
        #entrylist = list(page)
        text = Text.objects.all()
        audio = Audio.objects.all()
        video = Video.objects.all()
        serializer = PageSerializer(page, many=True)
        serializer_text = TextSerializer(text, many=True)
        serializer_audio = AudioSerializer(audio, many=True)
        serializer_video = VideoSerializer(video, many=True)
        return Response({"page": serializer.data, "text": serializer_text.data, "audio": serializer_audio.data, "video": serializer_video.data})

    def delete(self, request):
        page = Page.objects.all()
        page.delete()
        return Response({
            "message": "All pages have been deleted"
        }, status=204)
