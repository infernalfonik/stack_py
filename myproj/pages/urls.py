from django.urls import path

from .views import TextView, PageView, AudioUploadView, VideoView, AllPageView, AllVideoView, AllAudioView, AllTextView


app_name = "pages"

urlpatterns = [
    path('text/', AllTextView.as_view()),
    path('text/<int:pk>/', TextView.as_view()),
    path('audio/', AllAudioView.as_view()),
    path('audio/<int:pk>/', AudioUploadView.as_view()),
    path('page/', AllPageView.as_view()),
    path('page/<int:pk>/', PageView.as_view()),
    path('video/', AllVideoView.as_view()),
    path('video/<int:pk>/', VideoView.as_view()),
]