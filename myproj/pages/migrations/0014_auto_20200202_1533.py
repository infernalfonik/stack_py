# Generated by Django 2.2.7 on 2020-02-02 11:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0013_auto_20200202_1440'),
    ]

    operations = [
        migrations.AlterField(
            model_name='audio',
            name='page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='audios', to='pages.Page'),
        ),
        migrations.AlterField(
            model_name='text',
            name='page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='texts', to='pages.Page'),
        ),
        migrations.AlterField(
            model_name='video',
            name='page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='videos', to='pages.Page'),
        ),
    ]
