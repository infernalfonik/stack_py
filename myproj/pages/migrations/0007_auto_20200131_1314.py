# Generated by Django 2.2.7 on 2020-01-31 09:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0006_auto_20200130_2202'),
    ]

    operations = [
        migrations.AddField(
            model_name='audio',
            name='page',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='audios', to='pages.Page'),
        ),
        migrations.AddField(
            model_name='video',
            name='page',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='videos', to='pages.Page'),
        ),
        migrations.AlterField(
            model_name='audio',
            name='file',
            field=models.FileField(upload_to=''),
        ),
        migrations.AlterField(
            model_name='video',
            name='file',
            field=models.FileField(upload_to=''),
        ),
    ]
