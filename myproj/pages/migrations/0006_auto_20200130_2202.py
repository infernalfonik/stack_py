# Generated by Django 3.0.2 on 2020-01-30 18:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0005_auto_20200130_2136'),
    ]

    operations = [
        migrations.CreateModel(
            name='Audio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bitrate', models.IntegerField()),
                ('file', models.FileField(blank=True, upload_to='')),
            ],
        ),
        migrations.DeleteModel(
            name='File',
        ),
    ]
