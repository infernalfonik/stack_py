from django.contrib import admin

from .models import Page, Text, Audio, Video

admin.site.register(Text)
admin.site.register(Page)
admin.site.register(Audio)
admin.site.register(Video)

