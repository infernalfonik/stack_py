from django.db import models
#import mutagen

class Page(models.Model):
    title = models.CharField(max_length=120, blank=False, null=False,)
    number = models.AutoField(primary_key=True)
    counter = models.PositiveIntegerField(default=0)
    def __str__(self):
        return self.title
        
    
class Text(models.Model):
    text_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=120, blank=False, null=False, )
    body = models.TextField(blank=False, null=False, )
    page = models.ForeignKey('Page', related_name='texts', on_delete=models.CASCADE, blank=False, null=False,)
    counter = models.PositiveIntegerField(default=0)
    def __str__(self):
        return self.title


class Audio(models.Model):
    title = models.CharField(max_length=120, blank=False, null=False,)
    audio_id = models.AutoField(primary_key=True)
    file = models.FileField(blank=False, null=False, )
    bitrate = models.IntegerField(blank=False, null=False, )
    page = models.ForeignKey('Page', related_name='audios', on_delete=models.CASCADE, blank=False, null=False,)
    counter = models.PositiveIntegerField(default=0)
    #f = mutagen.File('filename')
    #bitrate = f.info.bitrate / 1000
    def __str__(self):
        return self.title

class Video(models.Model):
    title = models.CharField(max_length=120, blank=False, null=False,)
    video_id = models.AutoField(primary_key=True)
    file = models.FileField(blank=False, null=False)
    subtitles = models.FileField(blank=True, null=True)
    page = models.ForeignKey('Page', related_name='videos', on_delete=models.CASCADE, blank=False, null=False,)
    counter = models.PositiveIntegerField(default=0)
    def __str__(self):
        return self.title


