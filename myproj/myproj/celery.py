import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_DEFAULT', 'myproj.settings')

app = Celery('myproj')
app.config_from_object('django.conf:settings')

app.autodiscover_tasks()